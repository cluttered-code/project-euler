/**
 * Largest Prime Factor
 * Problem 3
 * 
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * 
 * What is the largest prime factor of the number 600851475143 ?
 */
import { primeFactorsOf } from '../util/factor'

const start = Date.now()

const input = 600851475143

const factors = primeFactorsOf(input)

const result = [...factors]
  .reduce((max, current) => Math.max(max, current))

const duration = Date.now() - start

console.log(`Problem 3: ${result} (${duration}ms)`)