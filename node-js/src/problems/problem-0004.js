/**
 * Largest palindrome product
 * Problem 4
 * 
 * A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
 * 
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */
function isPalindrome(num) {
  const str = new String(num)
  for (let i = 0, j = str.length - 1; i < j; i++, j--) {
      if (str[i] !== str[j]) return false
  }
  return true
}


const start = Date.now()

let result = 0

for (let i = 100; i < 1000; i++) {
  for (let j = i; j < 1000; j++) {
    const product = i * j
    if (isPalindrome(product)) result = Math.max(result, product)
  }
}

const duration = Date.now() - start

console.log(`Problem 4: ${result} (${duration}ms)`)