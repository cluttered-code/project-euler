/**
 * Sum square difference
 * Problem 6
 * 
 * The sum of the squares of the first ten natural numbers is,
 * 
 * 1^2 + 2^2 + ... + 10^2 = 385
 * The square of the sum of the first ten natural numbers is,
 * 
 * (1 + 2 + ... + 10)^2 = 55^2 = 3025
 * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
 * 
 * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 */
function sumOfSquares(max) {
  let sum = 0
  for(let i = 1; i <= max; i++) {
    sum += Math.pow(i, 2)
  }
  return sum
}

function squareOfSums(max) {
  let sum = 0
  for(let i = 1; i <= max; i++) {
    sum += i
  }
  return Math.pow(sum, 2)
}


const start = Date.now()

const max = 100

const result = squareOfSums(max) - sumOfSquares(max)

const duration = Date.now() - start

console.log(`Problem 6: ${result} (${duration}ms)`)