/**
 * Smallest Multiple
 * Problem 5
 * 
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * 
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */
const start = Date.now()

const inc = 2520
let result = 0
let found = false


while (!found) {
  result += inc
  for (let i = 11; i <= 20; i++) {
    if (result % i !== 0) break
    found = i === 20
  }
}

const duration = Date.now() - start

console.log(`Problem 5: ${result} (${duration}ms)`)