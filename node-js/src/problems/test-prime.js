import { isPrime } from '../util/prime'

const startTime = new Date().getTime()

const num = 1000000

let prime = 0
let count = 2
for (let i = 6; count < num; i += 6) {
    if (isPrime(i-1)) {
        prime = i-1
        ++count
    }
    if (isPrime(i+1)) {
        prime = i+1
        ++count
    }
}

const duration = new Date().getTime() - startTime
console.log(`${num}th prime: ${prime} in ${duration}ms`)