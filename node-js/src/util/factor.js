/**
 * @param {number} num
 * @returns {Set<number>} 
 */
export const primeFactorsOf = (num) => {
    let current = num
    const primeFactors = new Set()

    if (current & 1 === 0) current = Math.floor(current / 2)

    for (let i = 3; i <= current / i; i += 2) {
        while (current % i === 0) {
            primeFactors.add(i)
            current = Math.floor(current / i)
        }
    }

    if (current > 1) primeFactors.add(current)

    return primeFactors
}

/**
 * @param {number} num
 * @returns {Set<number>} 
 */
export const factorsOf = (num) => {
    const factors = new Set()
    
    if (num === 1) {
        factors.add(1)
        return factors
    }

    let factor = 1
    let factoredNum = num
    while (factor < factoredNum) {
        if ( num % factor === 0) {
            factoredNum = Math.floor(num / factor)
            factors.add(factor)
            factors.add(factoredNum)
        }
        factor += 1
    }

    return factors
}