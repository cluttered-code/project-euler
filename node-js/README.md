## Dependencies
```bash
yarn
```

## Run Problem
```bash
cd src/problems/
node -r ems problem-0001.js
```